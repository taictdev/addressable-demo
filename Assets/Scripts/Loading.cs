using Skywatch.AssetManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class Loading : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private LoadingAnimLoop _animLoop;
    [SerializeField] private Button _ReloadButton;
    private Dictionary<int, ConfigData> _assets = new Dictionary<int, ConfigData>();
    private void Awake()
    {
        _ReloadButton.onClick.AddListener(Reload);
    }
    private void Start()
    {
        Addressables.InitializeAsync().Completed += OnCompleted;
    }

    private void OnCompleted(AsyncOperationHandle<IResourceLocator> handle)
    {
        Debug.Log($"InitializeAsync {handle.Status}");
        if (handle.Status == AsyncOperationStatus.Succeeded)
        {
            LoadAssetDefault();
        }
    }

    public AsyncOperationHandle LoadAssetDefault()
    {
        _assets.Clear();
        _animLoop.Show();
        AsyncOperationHandle loader = AssetManager.LoadAssetsByLabelAsync("default");
        loader.Completed += op =>
        {
            List<object> results = op.Result as List<object>;
            Debug.Log($"LoadAssetDefault Completed {results.Count}");

            int id = 1;
            foreach (object obj in results)
            {
                ConfigData item = obj as ConfigData;
                Debug.Log($"LoadAssetDefault {item.name}");
                if (item == null)
                    continue;
                if (!_assets.ContainsKey(id))
                    _assets.Add(id, item);
                ++id;
            }

            Debug.Log($"LoadAssetDefault {_assets.Count}");
            if (_assets.TryGetValue(1, out ConfigData config))
            {
                SetImage(config.Sprite);
            }
            _animLoop.Hide();
        };
        return loader;
    }

    private void SetImage(Sprite sprite)
    {
        _image.gameObject.SetActive(true);
        _image.sprite = sprite;
    }

    private void Reload()
    {
        _image.gameObject.SetActive(false);
        LoadAssetDefault();
    }
}
