using UnityEngine;
using DG.Tweening;
public class LoadingAnimLoop : MonoBehaviour
{
    private void OnEnable()
    {
        AutoRotate();
    }
    private void OnDisable()
    {
        Kill();
    }
    private void OnDestroy()
    {
        Kill();
    }
    private void AutoRotate()
    {
        transform.DORotate(new Vector3(0, 0, -45), 0.25f, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear).SetId(this);
    }
    private void Kill()
    {
        DOTween.Kill(this);
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
